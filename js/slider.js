/**
 :: EDENTIC FULL SLIDE PLUGIN ::
**/

var methods = {
    elm: null,
    slideWidth: 0,
    containerWidth: 0,
    slide: 0,
    numberOfSlights: 0,

    drag: false,
    startPointX: 0,
    startPointY: 0,
    endPointX: 0,
    endPointY: 0,
    move: false,

    init: function(elm) {
        this.elm = elm;
        this.numberOfSlights = this.elm.find('slide').length - 1;

        methods.calcSpace();
        jQuery(window).resize(function() {
            methods.calcSpace();
        })

        jQuery('slide').click(function() {
            //methods.nextSlide();
        })

        jQuery('slide').on('touchstart', function(e) {
            //e.preventDefault();
            methods.startPointX = e.originalEvent.targetTouches[0].clientX;
            methods.startPointY = e.originalEvent.targetTouches[0].clientY;
            methods.elm.children('slider').addClass('drag');
            methods.drag = true;
        });

        jQuery('slide').on('touchend', function(e) {
            var end = methods.startPointX - methods.endPointX;
            methods.drag = false;
            methods.elm.children('slider').removeClass('drag');

                if(end > 100 && methods.move == true) {
                    methods.nextSlide();
                } else if(end < -100 && methods.move == true) {
                    methods.prevSlide();
                } else {
                    methods.animateSlide();
                }

        })

        jQuery('slide').on('touchmove', function(e) {
            if(methods.drag) {
                methods.endPointX = e.originalEvent.targetTouches[0].clientX;
                methods.endPointY = e.originalEvent.targetTouches[0].clientY;
                var calcX =  (e.originalEvent.targetTouches[0].clientX * 1.2) - (methods.startPointX * 1.2);
                if(Math.abs(calcX) > 10) {
                    e.preventDefault();
                    methods.move = true;
                    var slideX = (methods.slideWidth * methods.slide) * -1;
                    slideX += calcX;
                    methods.elm.children('slider').css('transform', 'translate3d(' + slideX + 'px,0,0)');
                }
            }

        })
    },

    calcSpace: function() {
        var slides = this.elm.find('slide').length;
        this.containerWidth = jQuery(window).width() * slides;
        this.slideWidth = jQuery(window).width();
        this.elm.children('slider').css({width: methods.containerWidth});
        this.elm.find('slide').css({width: methods.slideWidth});
    },

    nextSlide: function() {
        if(this.numberOfSlights == this.slide) {
            this.animateSlide();
            return;
        }

        this.slide += 1;
        this.animateSlide();
    },

    prevSlide: function() {
        if(this.slide == 0) {
            this.animateSlide();
            return;
        }

        this.slide-= 1;
        this.animateSlide();
    },

    animateSlide: function() {
        var slide = (this.slideWidth * this.slide) * -1;
        this.elm.children('slider').css('transform', 'translate3d(' + slide + 'px,0,0)');
    }
}

jQuery.fn.fullScreenSlide = function() {
    methods.init(this);
    return methods;
}
